package com.example.demo;

import com.example.xxx.RPoint;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class Receiver {

    public void receiveMessage(RPoint point) {
        log.info("Received: {}", point);
    }

}